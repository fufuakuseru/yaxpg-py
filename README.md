# yaxpg

[xkcd](https://xkcd.com/936/)-style passphrase generator like [xkpasswd](https://web.archive.org/web/20221207220016/https://xkpasswd.net/s/).

## Wordlist source

The various wordlist used in this project have been taken from this repository: <https://github.com/kkrypt0nn/wordlists>
