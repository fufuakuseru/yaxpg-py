import argparse
import importlib.metadata
import secrets
import sys
from enum import StrEnum, auto
from pathlib import Path
from typing import NamedTuple

import tomlkit
from loguru import logger
from appdirs import user_log_dir


class IncompatibleParameters(Exception):
    def __init__(self, params: dict):
        super().__init__()
        self.params = params
        self.params["wordlist_path"] = params["wordlist_path"].as_posix()


class AffixLength(NamedTuple):
    nb_symbols: int
    nb_digits: int

    def __str__(self):
        return (f"{self.nb_symbols} symbol{'s' if self.nb_symbols > 1 else ''}, "
                f"{self.nb_digits} digit{'s' if self.nb_digits > 1 else ''}")


class WordCase(StrEnum):
    LOWER = auto()
    UPPER = auto()
    CAPITALIZE = auto()
    ALTERNATING = auto()
    RANDOM = auto()
    LEET = auto()
    LEET_LOWER = auto()
    LEET_UPPER = auto()
    LEET_CAPITALIZE = auto()
    LEET_ALTERNATING = auto()
    LEET_RANDOM = auto()

    def convert(self, _input: str, start_upper: bool = bool(secrets.randbits(1))) -> str:
        logger.debug(f"converting '{_input}' to `{self}` casing")

        def _to_capitalize(_input: str) -> str:
            _output = ""
            for i, each_char in enumerate(_input):
                _output += each_char.upper() if i == 0 else each_char.lower()
            return _output

        def _to_alternating(_input: str) -> str:
            _output = ""
            is_upper = start_upper
            logger.debug(f"starting with {'an uppercase' if is_upper else 'a lowercase'} character")
            for each_char in _input:
                _output += each_char.upper() if is_upper else each_char.lower()
                is_upper = not is_upper
            return _output

        def _to_random(_input: str) -> str:
            _output = ""
            for each_char in _input:
                _output += each_char.upper() if bool(secrets.randbits(1)) else each_char.lower()
            return _output

        def _to_leet(_input: str) -> str:
            _output = ""
            leet_dict = {
                "a": "4",
                "e": "3",
                "g": "6",
                "i": "1",
                "l": "1",
                "o": "0",
                "s": "5",
            }
            try:
                i_idx = _input.lower().index("i")
                l_idx = _input.lower().index("l")
                if i_idx < l_idx:
                    logger.debug("found an 'i'/'I' first, therefore 'l'/'L' will be unchanged")
                    leet_dict.pop("l")
                else:
                    logger.debug("found an 'l'/'L' first, therefore 'i'/'I' will be unchanged")
                    leet_dict.pop("i")
            except ValueError:
                pass
            for each_char in _input:
                _output += leet_dict.get(each_char.lower()) or each_char
            return _output

        converters = {
            WordCase.LOWER           : str.lower,
            WordCase.UPPER           : str.upper,
            WordCase.CAPITALIZE      : _to_capitalize,
            WordCase.ALTERNATING     : _to_alternating,
            WordCase.RANDOM          : _to_random,
            WordCase.LEET            : _to_leet,
            WordCase.LEET_LOWER      : lambda _input: _to_leet(_input.lower()),
            WordCase.LEET_UPPER      : lambda _input: _to_leet(_input.upper()),
            WordCase.LEET_CAPITALIZE : lambda _input: _to_leet(_to_capitalize(_input)),
            WordCase.LEET_ALTERNATING: lambda _input: _to_leet(_to_alternating(_input)),
            WordCase.LEET_RANDOM     : lambda _input: _to_leet(_to_random(_input)),
        }
        return converters[self](_input)


class Parameters:
    def __init__(
            self,
            wordlist_path: Path,
            minimum_word_length: int | None = None,
            maximum_word_length: int | None = None,
            nb_words: int = 4,
            nb_passphrases: int = 1,
            case: WordCase | None = None,
            separator_length: int = 1,
            affix_length: tuple[int, int] = AffixLength(2, 2),
            separator: str | None = None,
            prefix: str | None = None,
            suffix: str | None = None,
            minimum_pass_length: int | None = None,
            maximum_pass_length: int | None = None,
            symbols_choice: list[str] | None = None,
            digits_choice: list[str] | None = None,
            read_config: Path | None = None,
            write_config: Path | None = None,
            verbose: int = 0,
    ):
        self.wordlist_path = wordlist_path
        self.nb_words = nb_words
        self.nb_passphrases = nb_passphrases
        if separator_length is None:
            assert (separator is not None)
            self.separator_length = len(separator)
        else:
            self.separator_length = separator_length
        if prefix is None or suffix is None:
            self.affix_length = affix_length
        self.prefix_length = len(prefix) if prefix is not None else affix_length[0] + affix_length[1]
        self.suffix_length = len(suffix) if suffix is not None else affix_length[0] + affix_length[1]
        self.separator = separator
        self.prefix = prefix
        self.suffix = suffix
        self.symbols_choice = symbols_choice or ["!", "@", "$", "^", "&", "*", "-", "_", "+", "|", "~", "?", "/", ".", ";"]
        self.digits_choice = digits_choice or [str(x) for x in range(10)]
        self.case = case or WordCase.RANDOM

        if minimum_word_length is None:
            if minimum_pass_length is None:
                self.minimum_word_length = 3
            else:
                computed_min = ((minimum_pass_length
                                 - self.nb_words * self.separator_length
                                 - self.prefix_length
                                 - self.suffix_length) // self.nb_words) - 1
                self.minimum_word_length = 0 if computed_min < 0 else computed_min
        else:
            self.minimum_word_length = minimum_word_length

        if maximum_word_length is None:
            if maximum_pass_length is None:
                self.maximum_word_length = 9
            else:
                computed_max = ((maximum_pass_length
                                 - self.nb_words * self.separator_length
                                 - self.prefix_length - self.suffix_length) // self.nb_words) + 1
                self.maximum_word_length = 0 if computed_max < 0 else computed_max
        else:
            self.maximum_word_length = maximum_word_length

        self.minimum_pass_length = minimum_pass_length
        self.maximum_pass_length = maximum_pass_length
        if minimum_pass_length and self.minimum_possible_length < minimum_pass_length:
            logger.error(f"minimum_possible_length ({self.minimum_possible_length}) < minimum_pass_length ({minimum_pass_length})")
            raise IncompatibleParameters(self.__dict__)
        if maximum_pass_length and self.maximum_possible_length > maximum_pass_length:
            logger.error(f"max_possible_length ({self.maximum_possible_length}) > maximum_pass_length ({maximum_pass_length})")
            raise IncompatibleParameters(self.__dict__)

        self.read_config = read_config
        self.write_config = write_config

        self.verbose = verbose
        logger.remove()
        logfile = Path(user_log_dir()) / "yaxpg-py.log"
        logger.add(logfile, level="DEBUG", backtrace=True, rotation="10 MB", compression="zip")
        if not self.verbose:
            logger.add(sys.stderr, level="WARNING", colorize=True)
        if self.verbose == 1:
            logger.add(sys.stdout, level="DEBUG", colorize=True)
        logger.debug(f"logging to '{logfile}'")

    def open_and_filter_wordlist(self) -> list[str]:
        logger.debug(f"reading {self.wordlist_path}")
        wordlist_content = self.wordlist_path.read_text()
        words = wordlist_content.splitlines()
        logger.debug(f"{len(words)=}")
        filtered_words = list(filter(lambda each_word: self.minimum_word_length <= len(each_word) <= self.maximum_word_length, words))
        logger.debug(f"{len(filtered_words)=}")
        return filtered_words

    def generate_affix(self, is_prefix: bool = True) -> str:
        logger.debug(f"generating a {'prefix' if is_prefix else 'suffix'}")
        symbols = [secrets.choice(self.symbols_choice) for _ in range(self.affix_length[0])]
        digits = [secrets.choice(self.digits_choice) for _ in range(self.affix_length[1])]
        if is_prefix:
            return "".join(symbols) + "".join(digits)
        else:
            return "".join(digits) + "".join(symbols)

    def generate(self):
        filtered_wordlist = self.open_and_filter_wordlist()
        passphrases = []
        for idx in range(self.nb_passphrases):
            logger.debug(f"{idx + 1}/{self.nb_passphrases}")
            prefix = self.prefix or self.generate_affix(True)
            suffix = self.suffix or self.generate_affix(False)
            separator = self.separator or "".join(secrets.choice(self.symbols_choice) for _ in range(self.separator_length))

            words = [self.case.convert(secrets.choice(filtered_wordlist)) for _ in range(self.nb_words)]
            passphrase = separator.join([prefix] + words + [suffix])
            passphrases.append(passphrase)

        return passphrases

    def output_to_toml(self, config: Path):
        config.parent.mkdir(parents=True, exist_ok=True)
        params_dict = self.__dict__
        filtered_params = {}
        for k, v in params_dict.items():
            if v is not None:
                if isinstance(v, Path):
                    logger.debug(f"{v} is a Path, writing it in posix format: {v.as_posix()}")
                    v = v.as_posix()
                filtered_params[k] = v
        toml_content = tomlkit.dumps({"yaxpg": filtered_params}, sort_keys=True)
        config.write_text(toml_content)
        logger.info(f"Parameters written to {config}")

    @property
    def minimum_possible_length(self):
        return (
                self.prefix_length
                + self.suffix_length
                + self.nb_words * self.separator_length
                + self.nb_words * self.minimum_word_length
        )

    @property
    def maximum_possible_length(self):
        return (
                self.prefix_length
                + self.suffix_length
                + self.nb_words * self.separator_length
                + self.nb_words * self.maximum_word_length
        )

    def __str__(self):
        as_dict = self.__dict__
        max_key_length = max([len(x) for x in as_dict.keys()])
        lines = []
        for key, value in as_dict.items():
            if value is not None:
                if "choice" in key:
                    value_str = "".join(value)
                elif isinstance(value, Path):
                    value_str = value.as_posix()
                else:
                    value_str = str(value)
                lines.append(f"{key:{max_key_length}}: {value_str}")
        return "\n".join(lines)

    def __repr__(self) -> str:
        return str(vars(self))


def str_to_affix_length(_input: str) -> AffixLength:
    if "," in _input:
        split = _input.split(",")
        if len(split) > 2:
            raise ValueError("Found more than 2 value")
        return AffixLength(nb_symbols=int(split[0].strip()), nb_digits=int(split[1].strip()))
    else:
        return AffixLength(nb_symbols=int(_input), nb_digits=int(_input))


def update_params_from_toml(params: Parameters, config: Path) -> Parameters:
    logger.debug(f"updating parameters from {config.as_posix()}")
    config_content = config.read_text()
    parsed_content = tomlkit.parse(config_content)
    yaxpg_data = parsed_content.get("yaxpg", {})

    if isinstance(yaxpg_data, dict):
        params_dict = params.__dict__
        # Use None to avoid KeyError
        params_dict.pop("prefix_length", None)
        params_dict.pop("suffix_length", None)
        params_dict.update(yaxpg_data)
        return Parameters(**params_dict)
    else:
        raise TypeError(f"yaxpg section in {config} could not be parsed into a dictionary")


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument("wordlist_path",
                        type=lambda p: Path(p) if Path(p).is_absolute() else Path.cwd() / p,
                        help="Path to wordlist file"
                        )
    parser.add_argument("-m",
                        "--minimum-word-length",
                        type=int,
                        help="Minimum length of words",
                        )
    parser.add_argument("-M",
                        "--maximum-word-length",
                        type=int,
                        help="Maximum length of words",
                        )
    parser.add_argument("-n",
                        "--nb-words",
                        type=int,
                        default=4,
                        help="Amount of words in passphrases",
                        )
    parser.add_argument("-c",
                        "--case",
                        type=WordCase,
                        help=f"Which case the words used in the passphrase ({', '.join([x for x in WordCase])})",
                        )
    parser.add_argument("-N",
                        "--nb-passphrases",
                        type=int,
                        default=1,
                        help="Amount of passphrases generated",
                        )
    parser.add_argument("-S",
                        "--separator-length",
                        type=int,
                        default=1,
                        help="Length of separator",
                        )
    parser.add_argument("-a",
                        "--affix-length",
                        type=str_to_affix_length,
                        default=AffixLength(2, 2),
                        help="Amount of (symbols, digits) used in affixes (if a single value both will be set to it)",
                        )
    parser.add_argument("--separator",
                        type=str,
                        help="Set a specific separator",
                        )
    parser.add_argument("--prefix",
                        type=str,
                        help="Set a specific prefix",
                        )
    parser.add_argument("--suffix",
                        type=str,
                        help="Set a specific suffix",
                        )
    parser.add_argument("--minimum-pass-length",
                        type=int,
                        help="Minimum length of generated passphrases",
                        )
    parser.add_argument("--maximum-pass-length",
                        type=int,
                        help="Maximum length of generated passphrases",
                        )
    parser.add_argument("--symbols-choice",
                        type=lambda x: [y for y in x],
                        default=None,
                        help="A string containing the symbols to use. Each character will be treated as a separate symbol.",
                        )
    parser.add_argument("--digits-choice",
                        type=lambda x: [y for y in x],
                        default=None,
                        help="A string containing the digits to use. Each character will be treated as a separate digit.",
                        )
    parser.add_argument("-r",
                        "--read-config",
                        type=lambda p: Path(p) if Path(p).is_absolute() else Path.cwd() / p,
                        help="Path to config file to read (must be a TOML file with a 'yaxpg' section)",
                        )
    parser.add_argument("--write-config",
                        type=lambda p: Path(p) if Path(p).is_absolute() else Path.cwd() / p,
                        help="Path to config file to write",
                        )
    parser.add_argument("-q",
                        "--quiet",
                        help="Only outputs the generated passphrase(s)",
                        action="store_const",
                        const=-1,
                        dest="verbose",
                        )
    parser.add_argument("-v",
                        "--verbose",
                        help="Display debug messages",
                        action="store_const",
                        const=1,
                        dest="verbose",
                        default=0,
                        )
    parser.add_argument("-V",
                        "--version",
                        action="version",
                        version=f"yaxpg-py {importlib.metadata.version('yaxpg')}",
                        )

    try:
        args_dict = vars(parser.parse_args())
        params = Parameters(**args_dict)
        if params.read_config is not None:
            params = update_params_from_toml(params, params.read_config)
    except IncompatibleParameters as e:
        logger.opt(exception=e).error("The current parameters cannot generate a valid passphrase")
        return 1

    if params.verbose != -1:
        print(str(params))
        print()

        print(f"Generating {params.nb_passphrases} passphrase{'s' if params.nb_passphrases > 1 else ''}:")

    passphrases = params.generate()

    passphrase_index_padding = len(str(len(passphrases)))
    passphrase_length_padding = len(str(max([len(x) for x in passphrases])))
    for i, each_passphrase in enumerate(passphrases, start=1):
        if params.verbose == -1:
            print(each_passphrase)
        else:
            print(f"\tNo. {i:0>{passphrase_index_padding}} "
                  f"({len(each_passphrase):0>{passphrase_length_padding}} chars): {each_passphrase}")

    if params.write_config is not None:
        params.output_to_toml(params.write_config)

    return 0


if __name__ == "__main__":
    sys.exit(main())
