import pytest

from yaxpg.yaxpg import Parameters, WordCase, str_to_affix_length

WORDLIST_CONTENT: str = "\n".join((str(i) * i) for i in range(1, 10))


class TestWordCase:
    test_input: str = "Hello, World!"

    def test_lower(self):
        expected_ouput = "hello, world!"
        assert WordCase.LOWER.convert(self.test_input) == expected_ouput

    def test_upper(self):
        expected_ouput = "HELLO, WORLD!"
        assert WordCase.UPPER.convert(self.test_input) == expected_ouput

    def test_capitalize(self):
        expected_output = "Hello, world!"
        assert WordCase.CAPITALIZE.convert(self.test_input) == expected_output

    def test_alternating_starting_upper(self):
        expected_output = "HeLlO, wOrLd!"
        assert WordCase.ALTERNATING.convert(self.test_input, start_upper=True) == expected_output

    def test_alternating_starting_lower(self):
        expected_output = "hElLo, WoRlD!"
        assert WordCase.ALTERNATING.convert(self.test_input, start_upper=False) == expected_output

    def test_random(self):
        assert WordCase.RANDOM.convert(self.test_input).lower() == self.test_input.lower()

    def test_leet(self):
        expected_output = "H3110, W0r1d!"
        assert WordCase.LEET.convert(self.test_input) == expected_output

    def test_leet_l(self):
        expected_output = "h3110, w0r1d!"
        assert WordCase.LEET_LOWER.convert(self.test_input) == expected_output

    def test_leet_u(self):
        expected_output = "H3110, W0R1D!"
        assert WordCase.LEET_UPPER.convert(self.test_input) == expected_output

    def test_leet_c(self):
        expected_output = "H3110, w0r1d!"
        assert WordCase.LEET_CAPITALIZE.convert(self.test_input) == expected_output

    def test_leet_a_starting_upper(self):
        expected_output = "H3110, w0r1d!"
        assert WordCase.LEET_ALTERNATING.convert(self.test_input, start_upper=True) == expected_output

    def test_leet_a_starting_lower(self):
        expected_output = "h3110, W0R1D!"
        assert WordCase.LEET_ALTERNATING.convert(self.test_input, start_upper=False) == expected_output

    def test_leet_r(self):
        expected_output = "h3110, w0r1d!"
        assert WordCase.LEET_RANDOM.convert(self.test_input).lower() == expected_output.lower()


class TestOpenFilterWordlist:
    def test_open_and_filter_wordlist_no_size(self, tmp_path):
        wordlist = tmp_path / "wordlist.txt"
        wordlist.write_text(WORDLIST_CONTENT)
        params = Parameters(wordlist, minimum_word_length=0, maximum_word_length=10)
        filtered_words = params.open_and_filter_wordlist()
        assert filtered_words == WORDLIST_CONTENT.splitlines()


class TestStrToAffix:
    def test_str_to_affix_single_value(self):
        expected_output = 1, 1
        assert str_to_affix_length("1") == expected_output

    def test_str_to_affix_two_values(self):
        expected_output = 1, 2
        assert str_to_affix_length("1,2") == expected_output

    def test_str_to_affix_fail(self):
        with pytest.raises(ValueError):
            str_to_affix_length("1,2,1")


class TestGenerateAffix:
    def test_generate_prefix(self, tmp_path):
        wordlist = tmp_path / "wordlist.txt"
        wordlist.write_text(WORDLIST_CONTENT)
        params = Parameters(wordlist, affix_length=(1, 1), symbols_choice=["_"], digits_choice=["1"])
        expected_output = "_1"
        assert params.generate_affix(True) == expected_output

    def test_generate_suffix(self, tmp_path):
        wordlist = tmp_path / "wordlist.txt"
        wordlist.write_text(WORDLIST_CONTENT)
        params = Parameters(wordlist, affix_length=(1, 1), symbols_choice=["_"], digits_choice=["1"])
        expected_output = "1_"
        assert params.generate_affix(False) == expected_output
